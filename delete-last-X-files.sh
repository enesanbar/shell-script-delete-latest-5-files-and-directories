#!/usr/bin/env bash

echo -e "\n############################\n"`date`"\n############################"

# Delete the unextracted files
for zippath in $( ls | grep '.zip$' ); do
    # get the directory name by removing the extension .zip
    dirname=${zippath%%.*}

    # check if a directory exists with that name.
    if [[ ! -d $dirname ]]; then
        rm $zippath
    fi
done

echo -e "Disk Usage before"
echo;df -h

# how many latest files & directories to keep
count=5
number_of_files=`ls -l . | egrep -c '^-'`
delete_count=$(expr ${number_of_files} - ${count})

if [ "${delete_count}" -le 0 ]; then
    echo;echo "No files deleted There're only $(expr ${number_of_files}) versions available. Exiting..."
    exit
fi

for zippath in $( ls -t | grep '.zip$' | tail -n ${delete_count} ); do
    dirname=${zippath%%.*}
    rm ${zippath}
    rm -rf ${dirname}
done

echo;echo "$delete_count files and directories have been deleted!"

echo;echo -e "Disk Usage after"
df -h